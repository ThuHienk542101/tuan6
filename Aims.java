package hust.soict.hespi.aims;
import hust.soict.hespi.aims.order.*;
import java.util.ArrayList;
import hust.soict.hespi.aims.media.*;
import hust.soict.hespi.aims.utils.*;
import java.util.Scanner;
public class Aims {
	public static void printList(Media[] media) {
		for(int i=0; i<media.length; i++) {
			System.out.print((i+1)+". Media"+" - "+ media[i].getId());
			System.out.print(" - " + media[i].getTitle());
			System.out.print(" - " + media[i].getCategory());
			System.out.print(": " + media[i].getCost()+"$\n");
		}
	}
	public static void main(String[] args) {
		Order anOrder = new Order();
		Media media[] = new Media[4];
		media[0] = new Media(0, "The Lion King", "Animation", 19.95f);
		media[1] = new Media(1, "Star Wars", "Science Fiction", 24.95f);
		media[2] = new Media(2, "Aladdin", "Animation", 18.99f);
		media[3] = new Media(3, "Star Wars 2", "Science Fiction", 20.00f);
		
		do {
			int choose;
			Scanner sc = new Scanner(System.in);
			System.out.println("--------------------------------");
			System.out.println("1. Create neworder");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.print("Your choice is: ");
			choose = sc.nextInt();	
			switch(choose) {
				case 0:
					return;
				case 1:
					anOrder = new Order();
					break;
				case 2:
					printList(media);
					System.out.print("add item has id = ");
					int idAdd = sc.nextInt();
					for(int i=0; i<media.length; i++) {
						if(media[i].getId()==idAdd) {
							anOrder.addMedia(media[i]);
						}
					}
					break;
				case 3:
					System.out.print("remove item has id = ");
					int idRemove = sc.nextInt();
					anOrder.removeMedia(media[idRemove]);
					break;
				case 4:
					anOrder.printOrdered();
					break;
				default:
					System.out.println("Please choose a number: 0-1-2-3-4");
					break;
			}
		} while(true);
	}
}